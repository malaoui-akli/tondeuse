package com.tondeuse;

import com.tondeuse.donnees.Instructions;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TondeuseApplication implements CommandLineRunner{


	public static void main(String[] args) {
		SpringApplication.run(TondeuseApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Instructions instructions = new Instructions();
		instructions.traitement();

    }
	

}
