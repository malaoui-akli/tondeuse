package com.tondeuse.entites;

import com.tondeuse.exceptionHandler.ExceptionTondeuse;
import lombok.*;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString

public class Pelouse {

    static int maxX;
    static int maxY;
    private Coordonnees coordonnees;

    public Object[][] taillePeleuse(Coordonnees coordonnees) throws ExceptionTondeuse {
        if (!coordonnees.isCoordonneesValide(coordonnees)) {
            throw new ExceptionTondeuse("La pelouse n'est pas rectongulaire !");
        } else {
            setMaxX(coordonnees.getX());
            setMaxY(coordonnees.getY());
            Object[][] pelouse = new Object[coordonnees.getX()][coordonnees.getX()];
            System.out.println("Pelouse[" + coordonnees.getX() + "][" + coordonnees.getY() + "]");
            return pelouse;
        }

    }


    public static void setMaxX(int maxX) {
        Pelouse.maxX = maxX + 1;
    }

    public static void setMaxY(int maxY) {
        Pelouse.maxY = maxY + 1;
    }

    public static int getMaxX() {
        return maxX;
    }

    public static int getMaxY() {
        return maxY;
    }
}
