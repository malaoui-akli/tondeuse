package com.tondeuse.donnees;

import com.tondeuse.entites.*;
import com.tondeuse.exceptionHandler.ExceptionTondeuse;
import lombok.Getter;
import lombok.Setter;


import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Instructions {
    private int nombreTondeuses;
    private List<Tondeuse> tondeuses = new ArrayList<>();
    private Pelouse pelouse = new Pelouse();
    private Coordonnees coordonnes = new Coordonnees();
    List<String> listeInstructions = new ArrayList<>();


    /**
     * Lire les données du fichier
     * @throws ExceptionTondeuse
     */
    public void lireDonnees() throws ExceptionTondeuse {

        try {
            InputStream flux = new FileInputStream("src/main/resources/Instructions.txt");
            InputStreamReader lecture = new InputStreamReader(flux);
            BufferedReader buff = new BufferedReader(lecture);
            String ligne;

            while ((ligne = buff.readLine()) != null) {
                listeInstructions.add(ligne);
            }
            buff.close();

            taillePelouse();

        } catch (IOException e) {
            throw new ExceptionTondeuse("Le fichier introuvable");

        }
    }


    //Taille de pelouse
    public Object[][] taillePelouse() throws ExceptionTondeuse {

        String[] coordPelouse = listeInstructions.get(0).split(" ");
        coordonnes.setX(Integer.parseInt(coordPelouse[0]));
        coordonnes.setY(Integer.parseInt(coordPelouse[1]));
        return pelouse.taillePeleuse(coordonnes);
    }


    // Nombre de tondeuse
    public void nombreTondeuse() {
        if (((listeInstructions.size() - 1)) % 2 == 0) {
            setNombreTondeuses((listeInstructions.size() - 1) / 2);
        }
    }

    //creation des tondeuses
    public void traitement() throws ExceptionTondeuse {
        lireDonnees();
        nombreTondeuse();

        int ligne = 1;
        for (int i = 0; i != getNombreTondeuses(); i++) {

            String[] positionsTondeuse = listeInstructions.get(ligne).split(" ");
            String[] instructionsTondeuse = listeInstructions.get(ligne + 1).split("");

            Tondeuse tondeuse = new Tondeuse();
            tondeuse.initTondeuse(i, Integer.parseInt(positionsTondeuse[0]),
                    Integer.parseInt(positionsTondeuse[1]), positionsTondeuse[2]);
            // Ajout dans la liste des tondeuses
            tondeuses.add(tondeuse);


            for (int y = 0; y != instructionsTondeuse.length; y++) {
                            tondeuses.get(i).instructionTondeuse( instructionsTondeuse[y],
                        tondeuses.get(i).getTondeuseOrientation(), tondeuses.get(i).getPosXTondeuse(),
                        tondeuses.get(i).getPosYTondeuse());

            }

            ligne = ligne + 2;
        }
        getStatutToutesTondeuses();
    }

    public void getStatutToutesTondeuses() {
        for (int i = 0; i != getNombreTondeuses(); i++) {
            tondeuses.get(i).getStatutTondeuse();
        }
    }

    public int getNombreTondeuses() {
        return nombreTondeuses;
    }



}
