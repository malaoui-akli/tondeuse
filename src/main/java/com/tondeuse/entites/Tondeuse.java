package com.tondeuse.entites;


import com.tondeuse.exceptionHandler.ExceptionTondeuse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Tondeuse {
    /* Caractéristiques d'une tondeuse */
    private int numTondeuse;
    private int posXTondeuse;
    private int posYTondeuse;
    private String tondeuseOrientation;
    private Pelouse peleuse = new Pelouse();

    public void initTondeuse(int numTondeuse, int positionsTondeuseX, int positionsTondeuseY,
                             String tondeuseOrientation) {
        setNumTondeuse(numTondeuse);
        setPosXTondeuse(positionsTondeuseX);
        setPosYTondeuse(positionsTondeuseY);
        setTondeuseOrientation(tondeuseOrientation);
    }

    /**
     * La fontion retoune état de pelouse en finale
     */
    public void getStatutTondeuse() {
        System.out.println(getPosXTondeuse() + " " + getPosYTondeuse() + " " + getTondeuseOrientation());
    }

    /**
     * @param instruction
     * @param cardinale
     * @param posXTondeuse
     * @param posYTondeuse
     * @throws ExceptionTondeuse
     */
    public void instructionTondeuse(String instruction, String cardinale, int posXTondeuse, int posYTondeuse) throws ExceptionTondeuse {
        switch (instruction) {
            case "A":
                avancer(cardinale, posXTondeuse, posYTondeuse);
                break;
            case "G":
                pivoterGauche(instruction, cardinale);
                break;
            case "D":
                pivoterDroite(instruction, cardinale);
                break;

            default:
                throw new ExceptionTondeuse("direction n'existe pas");

        }
    }


    /**
     * La fonction permet d'avencer la tondeuse
     *
     * @param cardinale
     * @param posXTondeuse
     * @param posYTondeuse
     * @throws ExceptionTondeuse
     */
    void avancer(String cardinale, int posXTondeuse, int posYTondeuse) throws ExceptionTondeuse {
        switch (cardinale) {
            // NORTH
            case "N":
                if ((posYTondeuse + 1) <= peleuse.getMaxY()) {
                    setPosYTondeuse(posYTondeuse + 1);
                }
                break;
            // EAST
            case "E":
                if ((posXTondeuse + 1) <= peleuse.getMaxX()) {
                    setPosXTondeuse(posXTondeuse + 1);
                }
                break;
            // WEST
            case "W":
                if (posXTondeuse - 1 != -1) {
                    setPosXTondeuse(posXTondeuse - 1);
                }
                break;
            // SOUTH
            case "S":
                if ((posYTondeuse - 1) != -1) {
                    setPosYTondeuse(posYTondeuse - 1);
                }
                break;
            default:
                throw new ExceptionTondeuse("Orientation n'existe pas");
        }
    }


    /**
     * La fonction permet de pivoter la tondeuse à gauche
     *
     * @param instruction
     * @param cardinale
     * @throws ExceptionTondeuse
     */
    public void pivoterGauche(String instruction, String cardinale) throws ExceptionTondeuse {
        switch (cardinale) {
            //NORTH
            case "N":
                setTondeuseOrientation("W");
                break;
            //EST
            case "E":
                setTondeuseOrientation("N");
                break;
            //WEST
            case "W":
                setTondeuseOrientation("S");
                break;
            //SOUTH
            case "S":
                setTondeuseOrientation("E");
                break;
            default:
                setTondeuseOrientation(getTondeuseOrientation());
                throw new ExceptionTondeuse("Orientation n'existe pas  ");


        }
    }

    /**
     * La fonction permet de pivoter la tondeuse à droite
     *
     * @param instruction
     * @param cardinale
     * @throws ExceptionTondeuse
     */
    public void pivoterDroite(String instruction, String cardinale) throws ExceptionTondeuse {
        switch (cardinale) {
            //Nord
            case "N":
                setTondeuseOrientation("E");
                break;
            // Est
            case "E":
                setTondeuseOrientation("S");
                break;
            //Ouest
            case "W":
                setTondeuseOrientation("N");
                break;
            //Sud
            case "S":
                setTondeuseOrientation("W");
                break;
            default:
                setTondeuseOrientation(getTondeuseOrientation());
                throw new ExceptionTondeuse("Orientation n'existe pas  ");

        }

    }
}
