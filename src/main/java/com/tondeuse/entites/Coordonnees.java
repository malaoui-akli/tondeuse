package com.tondeuse.entites;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
public class Coordonnees {
    private int x;
    private int y;

    public boolean isCoordonneesValide(Coordonnees coords) {
        return coords.getX() >= 0 && coords.getY() >= 0 && coords.getX() <= this.x && coords.getY() <= this.y;

    }
}
